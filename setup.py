from setuptools import setup
setup(name='laserlabtools',
      version='0.1',
      description='Usefull python stuff for the laser lab',
      url='#',
      author='Jonathan Bollig',
      author_email='j.bollig@mpic.de',
      license='MIT',
      packages=['laserlabtools'],
      zip_safe=False)
