from bokeh.models import ColumnDataSource, Row, Column, Button, Toggle, Model, HoverTool  # type:ignore
from bokeh.io import curdoc
from bokeh.plotting import figure, show
from bokeh.layouts import column, row, gridplot, layout  # type: ignore

import time
from datetime import datetime
import numpy as np
import numpy.typing as npt  # type:ignore
from typing import Union, Optional, Literal, List, Callable  # type:ignore
from multiprocessing.shared_memory import SharedMemory # type:ignore

from johnnylab.measurement_devices import OOS, VirtualTypes
from johnnylab.mathematics import calc_gauss_center, array_interval
from johnnylab.lab_funcs import NumpyMemory  # type:ignore

def print_integrals(intensities, wvl):
    indices = array_interval(wvl, 970, 980)
    wvl976 = np.sum(intensities[indices])
    indices = array_interval(wvl, 1015, 1045)
    wvlASE = np.sum(intensities[indices])
    indices = array_interval(wvl, 1083, 1085)
    wvl1084 = np.sum(intensities[indices])
    indices = array_interval(wvl, 950, 980)
    wvlOther = np.sum(intensities[indices])
    
    total = wvl976 + wvlASE + wvl1084
    print(round(wvl976/total*100), round(wvlASE/total*100), round(wvl1084/total*100), "({})".format(round(wvlOther/total*100)))

class Plot:
    # subclassing from bokeh.models.plot might be sensible
    def __init__(self, data_source, cds: Model, title:str, y_name: str, x_axis_type: str, rolling_avg: Optional[Union[float, int]] = None) -> None:
        self.cds = cds
        self.data_source = data_source
        self.fig = figure(title=title, sizing_mode='stretch_both', background_fill_color="black", x_axis_type=x_axis_type, tools = "pan,wheel_zoom,undo,redo,save,box_zoom,reset")
        hover_tool = HoverTool(anchor='top')  # mode="vline",  is an option
        self.fig.add_tools(hover_tool)
        if rolling_avg is not None:
            self.fig.line(x='x', y='y_avg', source=cds, line_width=3)
            self.fig.line(x='x', y='y', source=cds, line_width=3, line_alpha=0.333)
        else:
            self.fig.line(x='x', y='y', source=cds, line_width=3, line_alpha=1)

        self.fig.yaxis.axis_label = y_name

    def update(self) -> None:
        raise NotImplementedError

    def update_rolling_avg(self) -> None:
        raise NotImplementedError


class TimeSeriesPlot(Plot):
    # A subclass of a Plot, that shows the time evolution of a float
    def __init__(self, title:str, y_name:str, rollover_length: Optional[int], data_source: Model, func: Callable, rolling_avg: Optional[int] = None) -> None:
        cds = ColumnDataSource(data={'x':[], 'y':[], 'y_avg':[]})
        super().__init__(data_source, cds, title, y_name, x_axis_type="datetime", rolling_avg=rolling_avg)
        self.rollover_length = rollover_length
        self.func = func
        self.rolling_avg = rolling_avg

    def update(self):
        try:
            value = self.func(self.data_source)
        except Exception as e:
            value = None
            print("Value in TimeSeriesPlot could not be updated due to following error: {}".format(e))
        if value is not None:
            value_avg = np.nan
            if self.rolling_avg is not None:
                value_avg = np.average(np.concatenate([self.cds.data["y"][-self.rolling_avg-1:], [value]]))
            self.cds.stream({"y_avg": [value_avg], "y": [value], "x": [datetime.now()]}, rollover=self.rollover_length)


class SpectralPlot(Plot):
    # A subclass of a Plot, that shows spectrum-like data
    # with updating, intensities and wavelengths are always updates. For efficiency only updating intensities may be an option in the future. 
    def __init__(self, data_source, title: str, x_name: str, y_name: str, y_start_zero: bool = False, rolling_avg: Optional[float] = None) -> None:
        cds = ColumnDataSource(data={'x':[], 'y':[], 'y_avg':[]})
        super().__init__(data_source, cds, title, y_name, x_axis_type="auto", rolling_avg=rolling_avg)
        self.fig.xaxis.axis_label = x_name
        if y_start_zero:
            self.fig.y_range.start = 0
        self.rolling_avg = rolling_avg
        self.counts_avg = None

    def update(self):
        wavelengths, counts = self.data_source.read()
        if self.rolling_avg is not None:
            if self.counts_avg is None:
                self.counts_avg = counts
            else:
                self.counts_avg = (1-self.rolling_avg)*self.counts_avg + self.rolling_avg*counts
        if self.counts_avg is None:
            self.counts_avg = np.full_like(counts, np.nan)
        self.cds.data = {'x': wavelengths, 'y': counts, "y_avg": self.counts_avg}

class BokehSpectrometer:
    def __init__(self) -> None:
        self.prev_time = time.perf_counter()
        self.plots: List[Plot] = []
        self.layoutDOM: Column
    
    def update_chart(self):
        for plot in self.plots:
            plot.update()

    def add_plots(self, plots: List[Plot]):
        for plot in plots:
            self.plots.append(plot)

    def create_DOM(self, plots: List[List[Plot]]):
        rows = [row([plot.fig for plot in _row], sizing_mode='stretch_both') for _row in plots]  # type: ignore
        self.layoutDOM = column([_row for _row in rows], sizing_mode='stretch_both')


virtual: VirtualTypes = False
integration_millies: float = 10
loop_millies = 55  # callback frequency for the server loop (not sure if < 50 is sensible)
rollover_num: float = int(30*1000 / loop_millies)  # int(<seconds>*1000 / loop_millies)

# my_source = OOS(integration_millies, averages=1, virtual="gauss_with_noise")  # not sure what the point in averaging is
try:
    my_source = NumpyMemory("spec", create=False)
except FileNotFoundError:
    print("!!!!!! WARNING:")
    # my_source = OOS(integration_millies, averages=1, virtual="empty")
    my_source = OOS(integration_millies, averages=1, virtual=False)

spectral_plot = SpectralPlot(my_source, title = "Live Spectrum", x_name="Wavelength [nm]", y_name="Intensity [a.u.]", y_start_zero=False, rolling_avg=0.3)
# center_wvl_plot = TimeSeriesPlot("Peak location", "Peak wavelength",  rollover_num, spectral_plot.cds, lambda cds: calc_gauss_center(cds.data['x'], cds.data['y'], 0, 100, 0.3), rolling_avg=10)
integral_plot = TimeSeriesPlot("Peak integral", "Integral",  rollover_num, spectral_plot.cds, lambda cds: np.max(cds.data['y']), rolling_avg=3)
bokeh_spectrometer = BokehSpectrometer()
bokeh_spectrometer.add_plots([spectral_plot, integral_plot]) # , center_wvl_plot, integral_plot])
bokeh_spectrometer.create_DOM([[spectral_plot], [integral_plot]]) #  , [center_wvl_plot, integral_plot]])


if __name__ == "__main__":
    show(bokeh_spectrometer.layoutDOM)
else:
    curdoc().theme = 'dark_minimal'
    curdoc().add_periodic_callback(bokeh_spectrometer.update_chart, loop_millies)
    curdoc().add_root(bokeh_spectrometer.layoutDOM)
    curdoc().title = "Nice Spectrometer"
