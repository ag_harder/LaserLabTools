"""
Created by Jonathan Bollig
some time 2023

To-do:
    - most functions now require np.float64 arrays, because it works for typing. Proper implementation to be done
    - typing for calc_center_wavelength
    - fix the  # type:ignore
"""

from typing import Optional, Tuple
from scipy.signal import convolve # type:ignore
from astropy import modeling # type: ignore
import numpy as np
import numpy.typing as npt
import time  # type:ignore

def gauss(x: npt.NDArray[np.float32], a: float, x0: float, sigma: float) -> npt.NDArray[np.float32]:  # parameter sind amplitude, x-Ort des Maximum und Sigma-Breite
    return a*np.exp(-(x-x0)**2/(2*sigma**2))

def gaussian_convolution(y: npt.NDArray[np.float64], convolution_width: int = 11) -> npt.NDArray[np.float64]: # written mainly by chatgpt
    kernel_size = convolution_width
    sigma = 2
    x = np.linspace(-(kernel_size-1)/2, (kernel_size-1)/2, kernel_size)
    kernel = np.exp(-x*2 / (2*sigma*2))
    kernel /= np.sum(kernel)
    return convolve(y, kernel, mode='same') # type:ignore

def fit_gauss(x: npt.NDArray[np.generic], y: npt.NDArray[np.generic], 
              start: Optional[int] = None, stop: Optional[int] = None, 
              convolution_width: int = 11, max_iterations: int = 100) -> Tuple[float, float, float]:
    # returns mean (center of gaus), sigma (width of gaus) and height.
    # If fitting fails, the calculated values in the beginning are returned. 
    if start is not None:
        x = x[start:]
    if stop is not None:
        y = y[:stop]

    # perform convolution to smooth the y-array to find better maximum and x_0 approx.
    if convolution_width != 0:
        y = gaussian_convolution(y, convolution_width=convolution_width)

    mean: float = x[np.argmax(y)]
    sigma: float = np.sqrt(abs(sum(y * (x - mean)**2) / sum(y)))  # abs was added later to avoid negative values in sqrt
    height: float = max(y)

    fitter = modeling.fitting.LevMarLSQFitter()
    model = modeling.models.Gaussian1D(amplitude=height, mean=mean, stddev=sigma/2)
    try:
        fitted_model = fitter(model, x, y, maxiter=max_iterations) # type:ignore
        height: float = float(fitted_model.amplitude.value) # type:ignore
        mean: float = float(fitted_model.mean.value) # type:ignore
        sigma: float = float(fitted_model.stddev.value) # type:ignore
    except modeling.fitting.NonFiniteValueError as _:
        print("@fitgauss: WARNING: NonFiniteValueError. This means something with the gaussian fit is not going good")
    return height, mean, sigma # type:ignore

"""def center_of_mass(x: npt.NDArray[np.generic], y: npt.NDArray[np.float32], peak_analysis: float) -> np.float64:
    new_x: npt.NDArray[np.generic] = np.linspace(x[0], x[-1], 100)
    new_y: npt.NDArray[np.generic] = np.interp(new_x, x, y)
    peak_part = np.where(new_y > np.max(new_y)*peak_analysis)[0]
    return np.sum(new_y[peak_part] * new_x[peak_part])/np.sum(new_y[peak_part])
"""
def return_consecutive(array: npt.NDArray[np.intp], value: int):
    corr_vals = [value]
    
    stop = False
    curr_pos: int = value + 1
    while not stop:
        if curr_pos in array:
            corr_vals.append(curr_pos)
            curr_pos += 1 
        else: 
            stop = True
    stop = False
    curr_pos = value - 1
    while not stop:
        if curr_pos in array:
            corr_vals.insert(0, curr_pos)
            curr_pos -= 1 
        else: 
            stop = True
    return corr_vals

def array_interval(x: npt.NDArray[np.generic], start: Optional[float] = None, stop: Optional[float] = None) -> npt.NDArray[np.generic]:
    # returns indices of aan array with accending values in the given interval
    # usage: part = arr[array_interval(arr, start, stop)]
    if start is None:
        start: float = -np.inf
    if stop is None:
        stop: float = np.inf 
    return np.where((x >= start) & (x <= stop)) 

def calc_gauss_center(x: npt.NDArray[np.generic], y: npt.NDArray[np.generic], 
                           start: Optional[float] = None, stop: Optional[float] = None, dominance: Optional[float] = None) -> float:
    """
    x, y: Wavelengths and intensities of spectrum
    start, stop: interval in which spectrum should be considered (in units of wavelength, not indices)
    dominance: between [0,1) --> all data is considered. 1 --> only highest intensity is considered
    """
    index = array_interval(x, start, stop)
    x = x[index]
    y = y[index]
    if dominance is not None:
        index = np.where((y >= dominance*np.max(y)))
        x = x[index]
        y = y[index]
    gaus_fit = fit_gauss(x, y, convolution_width=0, max_iterations=15)
    center_wvl = gaus_fit[1]
    if center_wvl > x[-1]:
        center_wvl = x[-1]
    if center_wvl < x[0]:
        center_wvl = x[0]
    return center_wvl