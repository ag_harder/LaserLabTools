""""
Written by Jonathan Bollig
11.2023 (ca.)

for extensive notes on pyvisa, SCPI check obsidian, in short:
    - Pyvisa / SCPI tutorial: https://goughlui.com/2021/03/28/tutorial-introduction-to-scpi-automation-of-test-equipment-with-pyvisa/
    - PM100USB Manual: https://seltokphotonics.com/upload/iblock/6ae/6ae6adb1c9812f0eccbeeff7655e14d0.pdf
    - PM400 Manual: https://www.thorlabs.com/drawings/1c310c242e008b69-50DD38C3-AF0B-B995-56A24601980C3A3B/PM400-Manual.pdf

General notes:
    - Beam diameter only seems to be important to density calculations, so ignored here
    - PM400: USB0::4883::32885::P5001646::0::INSTR
    - PM100USB: USB0::4883::32882::P2001475::0::INSTR


What still could be implemented:
Powermeter:
    - hot plugging of sensor and powermeter
    - better validation for zeroing success (reading error files or similar)
    - not sure what happens if the range is changed automatically, and during that time, a power reading is done, could make an errror
    - Timeout and catching of different errors when calling __initialize_powermeter
    - MeasurementDevice could be implemented using abs (abstract base class)
    - sort out the # type:ignore
Spectrometer:
    - make averages not a parameter of read_intensities
    - most functions now require np.float64 arrays, because it works for typing. Proper implementation to be done
    - Check why SeaBreezeError doesn't want to be imported
    - check out: https://github.com/jonathanvanschenck/python-seatease 
General:
"""

import pyvisa  # conda install pyvisa, install https://www.ni.com/de/support/downloads/drivers/download.ni-visa.html#494653
import time
from typing import Optional, Literal, Tuple, Type, Union, List
from types import TracebackType
import numpy as np
import numpy.typing as npt

from seabreeze.spectrometers import Spectrometer as OceanSpectrometer # pip install seabreeze
from seabreeze.cseabreeze._wrapper import SeaBreezeError # type:ignore
from johnnylab.mathematics import gauss
from johnnylab.usefull_stuff import sleep_millies


def get_visa_resource_names():
    rm = pyvisa.ResourceManager()
    print(rm.list_resources())
    
class MeasurementDevice(): # Baseclass for all measurement devices
    def __init__(self) -> None:
        pass

    def read(self) -> npt.NDArray[np.float32]:
        raise NotImplementedError
    
    """ # deprecated
    def read_size(self) -> int:
        raise NotImplementedError
    """
    def read_shape(self) -> tuple[int, int]:
        raise NotImplementedError

    def read_info(self) -> Union[npt.NDArray[np.string_], npt.NDArray[np.float32]]:
        # This assigns the Titles to columsn of the csv
        raise NotImplementedError
    
    def read_header(self) -> str:
        # this returns an arbitrary header
        raise NotImplementedError
        
    
    def exit(self) -> None:  # this is called in the Logging thread to clean everything up
        raise NotImplementedError


class VirtualPowermeter:
    # propably could implement this as a child class of the actual powermeter, but too lazy now
    def __init__(self, _) -> None:
        self.timeout: int = 5000

    def query(self, string: str) -> str:
        if string == "*IDN?":
            return "VirtualPowermeter"
        elif string == "SYSTEM:SENSOR:IDN?":
            return "virtual sensor,69420"
        elif string == "SENSE:CORRECTION:WAVELENGTH? MAX":
            return "1200"
        elif string == "SENSE:CORRECTION:WAVELENGTH? MIN":
            return "200"
        elif string == "SENSE:CORRECTION:COLLECT:ZERO:MAGNITUDE?":
            return "0.1"
        elif string == "SENSE:CORRECTION:COLLECT:ZERO:STATE?":
            return "0"
        elif string == "READ?":  # returns Power in [W]
            time.sleep(30/1000)
            return str(np.random.random())
        elif string == "*TST?":
            return "0"
        else:
            print("WARNING: {} was not implemented as a query in virtual powermeter, returning '0'".format(string))
        return "0"
    
    def write(self, _: str) -> None:
        return None

    def __getstate__(self):  # this makes the class non-seriazable (just like a real connected measuring device), thanks chat gpt :)
        raise TypeError(f'{self.__class__.__name__} is not serializable')


Powermeter = Union[pyvisa.resources.usb.USBInstrument, VirtualPowermeter]  # for typing
class TLPM(MeasurementDevice):  # ThorLabs Power Meter
    """
    Supported Powermeters: PM100USB, PM400 
    Tested with Sensors S130VC and S401C

    This class should be used as following to insure proper cleanup:
    with TLPM(...) as device:
        # more code here
    Inspiration for this has been taken from here: https://stackoverflow.com/questions/865115/how-do-i-correctly-clean-up-a-python-object
    
    """
    def __init__(self, recource_name: str, wavelength: float = 633, timeout: int = 1000, virtual: bool =False, average: int = 1) -> None:
        """
        Wavelenght is given in nm
        timeout is given in ms
        """
        self.power_meter: Powermeter
        self.power_meter_IDN: str
        self.power_meter, self.power_meter_IDN = self.__initialize_powermeter(recource_name, timeout, virtual)
        self.reset()
        self.self_test()
        self.sensor_IDN: Optional[str] = self.__initialize_sensor()  # None if no sensor detected
        self.set_power_unit_W()
        self.wavelength = wavelength
        self.set_wavelength(wavelength)
        self.set_auto_range_on()
        self.average = average
        self.set_average(average)
        self.virtual = virtual
        time.sleep(1)  # just chill for a second for everythign to settle

    def __enter__(self):
        return self
    
    def __exit__(self, exctype: Optional[Type[BaseException]], excinst: Optional[BaseException], exctb: Optional[TracebackType]) -> None:
        self.exit()

    def exit(self):
        # public version of exit in case TLPM is not used in with statement
        if type(self.power_meter) == pyvisa.resources.usb.USBInstrument:
            self.reset()
            self.power_meter.close()
            print("@TLPM: powermeter cleaned up successfully")

    def __initialize_powermeter(self, recource_name: str, timeout: int, virtual: bool) -> Tuple[Powermeter, str]:
        # different try excepts could be implemented here
        # timeout could be implemented here, however multiprocessing would be needed for that
        powermeter:Powermeter
        if virtual:
            powermeter = VirtualPowermeter(recource_name)
        else:
            powermeter = pyvisa.ResourceManager().open_resource(recource_name, read_termination="\n") # type: ignore # with read_terminate, returned strings, don't have a newline character at the end
        powermeter.timeout = timeout
        IDN: str = powermeter.query("*IDN?")
        powermeter.write("CONFIGURE:POWER")  # this makes it read power measurements  
        if IDN.startswith("Thorlabs,PM100USB") or IDN.startswith("THORLABS,PM400"):
            print("@TLPM: Successfully connected to powermeter (Name, SN): {} {} {}".format(*IDN.split(",")[:3]))
        elif IDN.startswith("VirtualPowermeter"):
            print("@TLPM: Successfully connected to a virtual powermeter")
        else:
            print("WARNING: connected to something else: {}".format(IDN))
            print("         continuing anyway...")
        return powermeter, IDN

    def __initialize_sensor(self) -> Optional[str]:
        sensor_IDN: str = self.power_meter.query("SYSTEM:SENSOR:IDN?")
        if sensor_IDN.startswith("no sensor"):
            print("@TLPM: Sensor initialization failed, no sensor detected")
            return None
        else:
            print("@TLPM: Sensor (name & SN):", *sensor_IDN.split(",")[:2])
            return sensor_IDN

    def re_initialize_sensor(self):
        # public version of __initialize_sensor
        self.sensor_IDN = self.__initialize_sensor()

    def set_wavelength(self, wavelength: float) -> None:  # Unit: nm
        self.wavelength = wavelength
        if self.sensor_IDN is None:
            print("@TLPM: no wavelength setting performed, due to missing sensor")
            return
        max_wvl = float(self.power_meter.query("SENSE:CORRECTION:WAVELENGTH? MAX"))
        min_wvl = float(self.power_meter.query("SENSE:CORRECTION:WAVELENGTH? MIN"))
        if wavelength < min_wvl:
            print("@TLPM: wavelength {} nm to small, set to {} nm".format(wavelength, min_wvl))
            wavelength = min_wvl
        if wavelength > max_wvl:
            print("@TLPM: wavelength {} nm to high, set to {} nm".format(wavelength, max_wvl))
            wavelength = max_wvl
        self.power_meter.write("SENSE:CORRECTION:WAVELENGTH {}".format(wavelength))

    def set_auto_range_on(self) -> None:
        self.power_meter.write("POWER:RANGE:AUTO 1")

    def set_average(self, average: int) -> None:
        self.average = average
        # when reading the power, the powermeter makes average measurements and averages them.
        self.power_meter.write("AVERAGE {}".format(str(average)))

    def set_power_unit_W(self) -> None:
        self.power_meter.write("POWER:UNIT W")

    def zero_sensor(self) -> None:
        if self.sensor_IDN is None:
            print("@TLPM: no zeroing performed, due to missing sensor")
            return
        old_magnitude = float(self.power_meter.query("SENSE:CORRECTION:COLLECT:ZERO:MAGNITUDE?"))
        self.power_meter.write("SENSE:CORRECTION:COLLECT:ZERO")
        while self.power_meter.query("SENSE:CORRECTION:COLLECT:ZERO:STATE?") == "1":  # waites for the zeroing process to finish (usually ~ 2 seconds)
            time.sleep(0.1)
        new_magnitude = float(self.power_meter.query("SENSE:CORRECTION:COLLECT:ZERO:MAGNITUDE?"))
        if old_magnitude == new_magnitude and not self.virtual:  # This could be for sure implemented better (reading errors or something like that) but to lazy for now
            print("TLPM: Zeroing likely failed, since old and new offset are both: {}".format(str(old_magnitude)))
        else:
            print("@TLPM: Zeroing finished: offset changed from {} to {} W(?)".format(str(old_magnitude), str(new_magnitude)))  # not sure if W

    def read(self, unit: Literal["W", "mW"] = "W") -> npt.NDArray[np.float32]:
        if self.sensor_IDN is None:
            print("@TLPM: no power reading performed, due to missing sensor")
            power = np.nan
        else:
            power = float(self.power_meter.query("READ?"))
            if unit == "W":
                power = power*1000
        return np.array([[power]], dtype=np.float32)

    """ # deprecated
    def read_size(self) -> int:
        return np.float32().itemsize
    """
    def read_shape(self):
        return (1, 1)

    def read_info(self) -> npt.NDArray[np.string_]:
        # this is what gets written into the header of the csv file, before read() is called
        return np.array(["Power [mW]"])
    
    def read_header(self) -> str:
        return "wavelength: {}, averages: {}, virtual: {}".format(self.wavelength, self.average, self.virtual)

    def reset(self) -> None:
        # resets the device to default settings 
        # called at creation and cleanup of device
        self.power_meter.write("*RST")

    def self_test(self) -> None:
        # not sure what this tests, but seems sensible :)
        result = self.power_meter.query("*TST?")
        if result == "0":
            return
        else:
            print("@TLPM: Selftest was executed and failed :( !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!)")

VirtualTypes = Union[bool, Literal["gauss_with_noise", "gauss", "empty"]]
class VirtualSpectrometer:  # Virtual spectrometer for OOS
    # class ment to be compatible with Spectrometer class from seabreeze, to create a simulation of a connected spectrometer
    # Therefore the usage of the function intensities() to return the value intensities_val
    def __init__(self, virtual_type: VirtualTypes = "gauss_with_noise") -> None:
        self.integration_time_micros_limits: tuple[int, int] = (int(0), int(1e6))
        self.integration_time_micros_val: float = 10 * 1000
        self.model: str = "virtual_spectrometer"
        self.virtual_type: VirtualTypes = virtual_type

    def integration_time_micros(self, value: float) -> None:
        self.integration_time_micros_val = value

    def intensities(self, correct_nonlinearity: bool = True, # pylint: disable=unused-argument # unnecessary parameters to be consistent with spectrometer (https://stackoverflow.com/questions/59136064/python-unused-argument-needed-for-compatibility-how-to-avoid-pylint-complainin)
                    correct_dark_counts: bool = True): # pylint: disable=unused-argument 
        sleep_millies(self.integration_time_micros_val / 1e3)
        if self.virtual_type == "gauss_with_noise":
            intensities = gauss(self.wavelengths(), a=10, x0=50, sigma=5) + np.random.randn(len(self.wavelengths()))
        elif self.virtual_type == "gauss":
            intensities = gauss(self.wavelengths(), a=10, x0=50, sigma=5)
        elif self.virtual_type == "empty":
            intensities = []
        else:
            print("WARNING: virtual_type {} in VirtualSpectrometer not supported, returning empty array for intensities".format(self.virtual_type))
            intensities = []
        return np.array(intensities, dtype=np.float32)

    def wavelengths(self) -> npt.NDArray[np.float32]:
        if self.virtual_type == "empty":
            return np.array([], dtype=np.float32)
        else:
            return np.linspace(0, 100, 300, dtype=np.float32)


Spectrometer = Union[OceanSpectrometer, VirtualSpectrometer]
class OOS(MeasurementDevice):  # Ocean Optics Spektrometer (uses a real or virtual spectrometer)
    def __init__(self, integration_millis: int = 10, averages: int = 1, virtual: VirtualTypes = False) -> None:
        self.virtual: VirtualTypes = virtual
        if not virtual:
            print('@OOS: trying to connect to spectrometer')
            try:
                self.device: Spectrometer = OceanSpectrometer.from_first_available()
                print('@OOS: successfully connected to spectrometer ' + self.device.model)
            except SeaBreezeError:
                print('@OOS: no spectrometer found, did not connect (SeaBreezeError). Creating virtual empty spectrometer')
                self.device: Spectrometer = VirtualSpectrometer(virtual_type="empty")
                self.virtual = True
        else:
            print('@OOS: creating virtual spectrometer')
            self.device: Spectrometer = VirtualSpectrometer(virtual)
        self.integration_lims: tuple[int, int] = self.device.integration_time_micros_limits
        self.averages = averages
        integration_micros = integration_millis * 1000
        self.integration_micros = integration_micros
        if integration_micros < self.integration_lims[0]:
            self.integration_micros: int = self.integration_lims[0]
            print('@OOS: Integration time to small, set to {} milliseconds'.format(self.integration_lims[0] / 1000))
        if integration_micros > self.integration_lims[-1]:
            self.integration_micros: int = self.integration_lims[-1]
            print('@OOS: Integration time to big, set to {} milliseconds'.format(self.integration_lims[-1] / 1000))
        self.device.integration_time_micros(integration_micros)

        self.wavelengths = self.read_wavelengths()

    def read_intensities(self) -> npt.NDArray[np.float32]:
        # this could for sure be written better without catching an exception, if self.device.intensities() returns empty np arrays :)
        intensities_list: List[npt.NDArray[np.float32]]= [self.device.intensities(correct_nonlinearity=True, correct_dark_counts=True) for _ in range(self.averages)]
        try:
            return np.average(np.array(intensities_list, dtype=np.float32), axis=0)
        except ZeroDivisionError:
            return np.array([], dtype=np.float32)

    def read_wavelengths(self) -> npt.NDArray[np.float32]:
        self.wavelengths: npt.NDArray[np.float32] = np.array(self.device.wavelengths(), dtype=np.float32)
        return self.wavelengths

    def read(self) -> npt.NDArray[np.float32]:  # returns wavelengths and intensities for logging purposes
        return np.stack([self.wavelengths, self.read_intensities()], dtype=np.float32)

    def read_info(self) -> npt.NDArray[np.float32]:  # for Logging purposes
        return self.wavelengths
    
    def read_shape(self):
        return (2, len(self.wavelengths))

    def read_header(self) -> str:
        return "integration_millies: {}, averages: {} virtual: {}".format(self.integration_micros/1000, self.averages, self.virtual)

    def exit(self):
        print("@OOS: exiting spectrometer :)")

def test_powermeter(recource_name: str, virtual: bool=False):
    print()
    print("Testing now powermeter (virtual: {}):".format(virtual))
    start = time.perf_counter()
    with TLPM(recource_name=recource_name, wavelength=1084, timeout=5000, virtual=virtual) as device:
        start = time.perf_counter()
        print("Elapsed time (init):", round((time.perf_counter()-start)*1000, 1), "ms")
        device.set_average(10)
        device.zero_sensor()
        device.read()
        device.read_info()
        device.read_shape()
        device.read_header()
        print("Power:", device.read("mW"), "mW")
        print("Elapsed time (tests):", round((time.perf_counter()-start)*1000, 1), "ms")

def test_spectrometer(virtual: bool=False):
    print()
    print("Testing now spectrometer (virtual: {}):".format(virtual))
    start = time.perf_counter()
    device = OOS(integration_millis=10, averages=3, virtual=virtual)
    print("Elapsed time (init):", round((time.perf_counter()-start)*1000, 1), "ms")
    start = time.perf_counter()
    device.read()
    device.read_info()
    device.read_shape()
    device.read_header()
    print("Elapsed time (tests):", round((time.perf_counter()-start)*1000, 1), "ms")

if __name__ == "__main__":
    # get_visa_resource_names()
    test_powermeter(recource_name="USB0::4883::32882::P2001475::0::INSTR", virtual=True)
    test_powermeter(recource_name="USB0::4883::32882::P2001475::0::INSTR", virtual=False)
    test_spectrometer(virtual=True)
    test_spectrometer(virtual=False)
