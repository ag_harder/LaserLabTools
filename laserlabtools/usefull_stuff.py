import time

"""
to be copy pasted:
- good time measurement in milliseconds:
    start = time.perf_counter()
    print("Ellapsed time: {} ms".format((time.perf_counter()-start)*1000))

"""

def sleep_millies(millies: float) -> None:
    # be aware: this is a processor intense sleeping
    now = time.perf_counter()
    end = now + millies/1000
    while now < end:
        now = time.perf_counter()