"""
Written by Jonathan Bollig
11.12.2023

To-do
    - Close Shared Memory at some point?
    - Understand why the size of the SharedMemory changes to ~4000
    - In Numpy Memory check if memory exists already
    - implement do all x milliseconds in logging loop
    - implement propper inheritance for measurement classes from super measurement class
    - most functions now require np.float64 arrays, because it works for typing. Proper implementation to be done
    - implement optional saving of data in logger

General Notes:
    - PM400: USB0::4883::32885::P5001646::0::INSTR
    - PM100USB: USB0::4883::32882::P2001475::0::INSTR

"""

from datetime import datetime
from typing import Union, Optional  # type: ignore
import csv
import time
import numpy.typing as npt
import numpy as np
from multiprocessing.shared_memory import SharedMemory
import threading
import struct

from johnnylab.measurement_devices import TLPM, OOS, MeasurementDevice  # type: ignore

DATETIME_FORMAT_DATA = r"%Y_%m_%d_%H_%M_%S.%f"
DATETIME_FORMAT_FILE = r"%Y_%m_%d_%H_%M_%S"

"""class NumpyMemory:
    def __init__(self, name: str, memory_size: int = 0, create: bool = True) -> None:
        self.name = name
        if create:
            try:
                self.memory = SharedMemory(name, create=True, size=memory_size)
                self.memory_size = memory_size
            except FileExistsError as e:
                print("@NumpyBuffer: WARNING: cannoct create memory '{}', since it already exists.".format(self.name))
                raise e
        else:
            try:
                self.memory = SharedMemory(name, create=False)
            except FileNotFoundError as e:
                print("WARNING: System could not find the following memory: {}".format(self.name))
                raise e

    def read(self) -> Optional[npt.NDArray[np.float32]]:  # returns a numpy array (prop. only 1D?)
        # 2. answer: https://stackoverflow.com/questions/7894791/use-numpy-array-in-shared-memory-for-multiprocessing 
        return np.frombuffer(self.memory.buf[:self.memory_size], dtype=np.float32)

    def write(self, data: npt.NDArray[np.float32]):
        self.memory.buf[:self.memory_size] = data.tobytes()

    def unlink(self):
        self.memory.close()
        self.memory.unlink()
        print("@NumpyBuffer: unlinked and closed {} memory".format(self.name))
"""

class NumpyMemory:
    # This only supports float32 2 dimensional numpy arrays
    def __init__(self, name: str, shape: tuple[int, int] = (0, 0), create: bool = True) -> None:
        self.name = name
        if create:
            try:
                self.shape = shape
                self.memory = SharedMemory(name, create=True, size=shape[0]*shape[1]*4 + 8)  # each flaot32 needs 4 bytes
            except FileExistsError as e:
                print("@NumpyBuffer: WARNING: cannoct create memory '{}', since it already exists.".format(self.name))
                raise e
            metadata = struct.pack('ii', shape[0], shape[1])
            self.memory.buf[:8] = metadata
        else:
            try:
                self.memory = SharedMemory(name, create=False)
            except FileNotFoundError as e:
                print("WARNING: System could not find the following memory: {}".format(self.name))
                raise e
            metadata = struct.unpack('ii', self.memory.buf[:8])
            self.shape = (metadata[0], metadata[1])

    def read(self) -> Optional[npt.NDArray[np.float32]]:  # returns a numpy array (prop. only 1D?)
        # 2. answer: https://stackoverflow.com/questions/7894791/use-numpy-array-in-shared-memory-for-multiprocessing 
        return np.ndarray(self.shape, dtype=np.float32, buffer=self.memory.buf[8:])

    def write(self, data: npt.NDArray[np.float32]):
        np_array = np.ndarray(self.shape, dtype=np.float32, buffer=self.memory.buf[8:])  # Leave space for metadata
        np.copyto(np_array, data)

    def unlink(self):
        self.memory.close()
        self.memory.unlink()
        print("@NumpyBuffer: unlinked and closed {} memory".format(self.name))




class MeasurementLogger(threading.Thread):
    # logs 1D-data into memory and or a csv file
    # device neads a read() and read_info() function
    def __init__(self, meas_device: MeasurementDevice, write_path: str, memory: NumpyMemory, print_value: bool = False, sleep: Optional[int] = None) -> None:
        super().__init__()
        self.sleep = sleep  # sleep time in loop in milliseconds
        self.device = meas_device
        self.write_path = write_path
        self.memory = memory
        self.start_time = None
        self.last_measurement = None
        self.shutdown = False
        self.print_value = print_value
    
    # @override
    def run(self):  # has to be called run to override the Thread method
        self.start_time = datetime.utcnow()
        write_name = self.write_path + self.start_time.strftime(DATETIME_FORMAT_FILE) + ".csv"
        with open(write_name, "w") as w:
            writer=csv.writer(w, delimiter=',',lineterminator='\n',)
            writer.writerow([self.device.read_header()])  # general header
            # writer.writerow([DATETIME_FORMAT_DATA, *self.device.read_info()])  # columns descriptors
            print("@MeasurementLogger: starting logging loop")
            last_time = time.perf_counter()
            while not self.shutdown:
                data = self.device.read()
                now = datetime.utcnow().strftime(DATETIME_FORMAT_DATA)
                writer.writerow([now, *data.flatten()])
                if self.sleep is not None:
                    time.sleep(self.sleep/1000)
                self.memory.write(data)
                self.last_measurement = data
                if self.print_value:
                    print(self.memory.name, (time.perf_counter()- last_time)*1000)
                last_time = time.perf_counter()
        self.device.exit()

    # @override
    def join(self, timeout=None):  # type: ignore because it is not set in parent class
        self.shutdown = True
        print("@MeasurementLogger: joining Thread")
        return super().join(timeout=timeout)

if __name__ == "__main__":
    # initialize all measurement devices etc
    write_path = "johnny_lab\\data\\"
    
    spectrometer = OOS(virtual=False, integration_millis=50)
    spec_memory = NumpyMemory("spec", spectrometer.read_shape())
    spec_logger = MeasurementLogger(meas_device=spectrometer, write_path=write_path + "spec", memory=spec_memory, print_value=True)
    spec_logger.start()
    
    powermeter = TLPM("USB0::4883::32882::P2001475::0::INSTR", 1084, 3000, average=100)
    power_memory = NumpyMemory("power", powermeter.read_shape())
    power_logger = MeasurementLogger(meas_device=powermeter, write_path=write_path + "power", memory=power_memory, print_value=True)
    power_logger.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt as e:
        power_logger.join()
        spec_logger.join()
        spec_memory.unlink()  # unlinking has to happen AFTER joining the threads
        power_memory.unlink()
    